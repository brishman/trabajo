<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Register extends Model{
    protected $table='registers';
    protected $primaryKey = 'id';
    protected $fillable = [
        'date',
        'dependence',
        'username',
        'location',
        'modality',
        'internal_code',
        'heritage_code',
        'brand',
        'model',
        'kind',
        'color',
        'serie',
        'description',
        'state',
        'freckled',
        'unity',
        'quantity',
        'price',
        'total'
    ];

}
