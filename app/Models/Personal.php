<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
//Traits\User;
class Personal extends Model
{
    protected $with = ['user'];
    protected $table='personal';
    protected $primaryKey = 'id';
    protected $fillable = [
        'number',
        'name',
        'surnames',
        'office',
        'position',
        'status'
    ];
    public function user(){
    return $this->hasOne(User::class,'id','users_id');
    }
}
