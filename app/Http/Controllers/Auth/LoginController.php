<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginFormRequest;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm(){
        return view('layouts.login.index');
    }

    public function login(Request $request){
         $this->validateLogin($request);
         if (Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
             return response()->json(['message'=>'Usuario Conectado','status'=>200]);
         }else{
             return response()->json(['message'=>'Cuenta de Usuario Incorecto','status'=>402]);
             return redirect('/');
         }
     }
     public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        return redirect('/');
     }
     protected function validateLogin(Request $request){
         $this->validate($request,[
           //  'tienda' => 'required',
             'email' => 'required|string',
             'password' => 'required|string'
         ]);
 
     }

}
