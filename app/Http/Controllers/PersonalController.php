<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Personal;
use App\User;
use App\Http\Resources\PersonalCollection;
use App\Http\Resources\PersonalResource;
use App\Http\Requests\PersonalRequest;
class PersonalController extends Controller
{
    public function  index(){ //Mostrar el Grid
        return view('layouts.personal.index');
    }
    public function  store(PersonalRequest $request){//Guardar
       
        if($request->id!=null){
            $users=User::updateOrCreate(['id' => $request->users_id],
            ['name'=>$request->username,
            'email'=> $request->email,
            'type'=>$request->type,
            'password'=>bcrypt( $request->password)]);
            $rows=Personal::updateOrCreate(['id' => $request->id],
            ['number'=>$request->number,
            'name'=>$request->name,
            'surnames'=> $request->surnames,
            'users_id'=>$request->users_id,
            'office'=> $request->office,
            'position'=> $request->position]);
        }else{          
                $users_id = $request->input('users_id');
                $name = $request->input('surnames');
                $type = $request->input('type');
                $user = User::firstOrNew(['id' => $users_id]);
                $user->fill($request->all());
                $user->save();
                $personal = Personal::firstOrNew(['id' => $request->input('id')]);
                $personal->users_id=  $user->id;
                $personal->status='1';
                $personal->fill($request->all());
                $personal->save();
        }

        return response()->json(['success'=>true,'msg'=>'Se Guardó con Exito'],201);
    }
    public function columns()//Opciones de Busqueda
    {
        return [
            'number' => 'Numero DNI',
            'name' => 'Nombres',
            'surnames' => 'Apellidos'
        ];
    }
    public function records(Request $request){ ///listar los registro
        $records = Personal::with('user')->where($request->column, 'like', "%{$request->value}%")->orderBy($request->column);
        return new PersonalCollection($records->paginate(config('items_per_page')));
    }
    public function record($id){ //Mostrar un solo registro
        return new PersonalResource(Personal::findOrFail($id));
         
    }
    public function baja(Request $request){
        $personal=Personal::findOrFail($request->id);
        $user=User::findOrFail($personal->users_id);
        $personal->delete();
        $user->delete();
       
         return response()->json(['success'=>true,'msg'=>'Se Elimino con Exito'],201);
    }
}
