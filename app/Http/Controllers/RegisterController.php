<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Register;
use App\Models\Personal;
use App\Http\Resources\RegisterCollection;
use App\Http\Resources\RegisterResource;
use PDF;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class RegisterController extends Controller
{
    public function  index(){
        return view('layouts.register.index');
    }
    public function columns()
    {
        return [
            'internal_code' => 'Codigo Interno',
            'heritage_code' => 'Codigo Patrimonial',
            'date' => 'Fecha de registro',
            'description' => 'Descripcion'
        ];
    }
    public function tables(){
        $personal = Personal::select('number','name','surnames','office','position','users_id',DB::raw("Concat(name,' ',surnames) AS nombres"))->get();
        return compact('personal');
    }
    public function records(Request $request){
        $records = Register::where($request->column, 'like', "%{$request->value}%")->orderBy('id');
        return new RegisterCollection($records->paginate(config('items_per_page')));

    }
    public function record($id){
        $record = new RegisterResource(Register::findOrFail($id));
        return $record;
    }
    public function report($tipo,$fecha1,$fecha2){
        $fecha = Carbon::now();
        $fecha = $fecha->format('d-m-Y');
        $registers=Register::whereBetween("date",[$fecha1,$fecha2])->get();
   if($tipo==1){
         $ticket=PDF::loadView('layouts.register.report', ['registers'=>$registers]);
   }else{
    $ticket=PDF::loadView('layouts.register.report2', ['registers'=>$registers,'fecha'=>$fecha]);
        $ticket->setPaper('A4', 'landscape');
   }    
    return $ticket->stream('tickets.pdf');
      }
    
    public function baja(Request $request){
        //dd($request);
        $register = Register::findOrFail($request->id);
        if($request->accion=="baja"){
            $register->state="Baja";
            $mensaje='Se dio de baja con Exito';
        }else{
            $register->state="Alta";
            $mensaje='Se dio de Alta con Exito';
        }
        $register->save();
        return response()->json(['success'=>true,'msg'=>$mensaje],201);
    }
    public function save(Request $request){
        try {
            DB::beginTransaction();
            $date = Carbon::now();
            $year=$date->format('Y');
//            dd();
         
            $register=Register::updateOrCreate(['id' => $request->id],
            ['date'=>$request->date,
          
            'dependence'=> $request->dependence,
            'username'=> $request->username,
            'location'=>$request->location,
            'modality'=>'Funcionario',
            'internal_code'=> $request->internal_code,
            'heritage_code'=> $request->heritage_code,
            'description'=> $request->description,
            'state'=> $request->state,
            'freckled'=> $request->freckled,
            'unity'=> $request->unity,
            'quantity'=> $request->quantity,
            'brand' => $request->brand,
            'model' => $request->model,
            'kind' => $request->kind,
            'color' => $request->color,
            'serie' => $request->serie,
            'year' => $request->year,
            'price'=>0.00,
            'total'=> 0.00]);
            DB::commit();
            return response()->json(['success'=>true,'msg'=>'Se Guardó con Exito'],201);

            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(['success'=>false,'msg'=>$e],201);
            }
    
    }

    public function destroy($id){

    }
}
