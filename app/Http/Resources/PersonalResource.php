<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PersonalResource extends JsonResource{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'number' => $this->number,
            'name' => $this->name,
            'email' => $this->user->email,
            'username'=>  $this->user->name,
          //  'password' => $this->user->password,
            'surnames' => $this->surnames,
            'office' => $this->office,
            'position' => $this->position,
            'users_id'=>$this->users_id,
        ];
    }
}