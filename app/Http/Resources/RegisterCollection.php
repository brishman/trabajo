<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\ResourceCollection;
class RegisterCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id' => $row->id,
                'date' => $row->date,
                'code' => $row->code,
                'internal_code'=> $row->internal_code,
                'state'=> $row->state,
                'heritage_code'=> $row->heritage_code,
                'freckled'=> $row->freckled,
                'description' => $row->description,
                'quantity' => $row->quantity,
                'brand' => $row->brand,
                'model' => $row->model,
                'kind' => $row->kind,
                'color' => $row->color,
                'serie' => $row->serie
            ];
        });
    }
}
