<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RegisterResource extends JsonResource{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'code' => $this->code,
            'dependence' => $this->dependence,
            'username' => $this->username,
            'location' => $this->location,
            'modality' => $this->modality,
            'internal_code' => $this->internal_code,
            'heritage_code' => $this->heritage_code,
            'description' => $this->description,
            'freckled' => $this->freckled,
            'state' => $this->state,            
            'unity' => $this->unity,            
            'quantity' => $this->quantity,
            'brand' => $this->brand,
            'model' => $this->model,
            'kind' => $this->kind,
            'color' => $this->color,
            'serie' => $this->serie,
            'price' => $this->price,
            'total' => $this->total
        ];
    }
}