<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\User;
class PersonalCollection extends ResourceCollection{

    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
          //  $user= User::all(); 
            return [
                'id' => $row->id,
                'number' => $row->number,
                'name' => $row->name,
                'surnames'=> $row->surnames,
                'email'=> $row->user->email,
                'username'=> $row->user->name,
                'office'=> $row->office,
                'position'=> $row->position,
                'users_id'=>$row->users_id,
            ];
        });
    }
}
