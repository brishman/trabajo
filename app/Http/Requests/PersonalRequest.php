<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PersonalRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = $this->input('id');
        return [
            'number' => ['required','numeric'],
            'name' => ['required'],
            'type' => ['required'],   
            'username' => ['required'],         
            'surnames' => ['required'],
            'office' => ['required'],
            'position' => ['required'],
            'email' => ['required','email'],
            'password' => ['required'],
           
            

        ];
    }
    
public function messages()
{
    return [
        'number.required' => 'El :attribute es obligatorio.',
        'type.required' =>  'El :attribute es obligatorio.',
        'name.required' =>  'El :attribute es obligatorio.',
        'username.required' =>  'El :attribute es obligatorio.',
        'surnames.required' => 'El :attribute es obligatorio.',
        'office.required' =>  'El :attribute es obligatorio.',
        'position.required' => 'El :attribute es obligatorio.',
        'email.required' =>  'El :attribute es obligatorio.',
        'password.required' =>  'El :attribute es obligatorio.',
       
    ];
}

public function attributes()
{
    return [
        'number' => 'N° de DNI',
        'name' => 'Nombre',
        'username' => 'Usuario',
        'type' => 'Tipo Usuaro',
        'surnames' => 'Apellidos',
        'office' => 'Dependecia',
        'position' => 'Dirección',
        'email' => 'Email',
        'password' => 'password',
    ];
}
}