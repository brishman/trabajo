<aside id="sidebar-left" class="sidebar-left">
				
                <div class="sidebar-header">
                    <div class="sidebar-title">
                        Menu Naveación
                    </div>
                    <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>
            
                <div class="nano">
                    <div class="nano-content">
                        <nav id="menu" class="nav-main" role="navigation">
                        
                            <ul class="nav nav-main">
                                <li>
                                    <a href="layouts-default.html">
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                        <span>Dashboard</span>
                                    </a>                        
                                </li>
                                <li class="nav-parent nav-expanded nav-active">
                                    <a href="#">
                                        <i class="fa fa-columns" aria-hidden="true"></i>
                                        <span>Layouts</span>
                                    </a>
                                    <ul class="nav nav-children">
                                    @if (auth()->user()->type=="1")

                                        <li>
                                            <a href="personal">
                                               Registro de Personal
                                            </a>
                                        </li>
                                        @endif     
                                        <li>
                                            <a href="registers">
                                                Registro de Inventario
                                            </a>
                                        </li>
                                      
                              
                                   
                                    
                                    </ul>
                                </li>
                                
                               
                                    </ul>
                                </li>

                        </nav>
            

                    </div>

            
                </div>
            
            </aside>