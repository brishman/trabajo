<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">	
	<title>Reporte de Recepcion y Entrega de Cargo </title>
<style type="text/css">
  @page {
    margin: 80px 20px 55px 20px;
}  
 body {
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
            font-size: 0.875rem;
            font-weight: normal;
            color: #151b1e;           
        }
            header {
                position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                text-align: center;
            }
 header,footer {
                position: fixed; 
                bottom: 0px; 
                left: 0px; 
                right: 0px;
                color:#000;
                text-align: center;
                font-size:12px;
                font-family:verdana; 
}   
table {
   width: 100%;
   text-align: left;
   border-collapse: collapse;
   caption-side: top;
}
caption, td,  {
   padding: 0.3em;
}
tbody {
   border-top: 1px solid #ddd;
   border-bottom: 1px solid #ddd;
}
.border-bottom{
    border-bottom: 1px solid #ddd;
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;  
}
tbody th, tfoot th {
   border: 0;
}
th{
    font-size:12px;
    padding:5px;
    background:#ddd;
    border-bottom: 1px solid #ddd;
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;  
}
th.name {
   width: 25%;
}
th.location {
   width: 20%;
}
th.lasteruption {
   width: 30%;
}
th.eruptiontype {
   width: 25%;
}
tfoot {
   text-align: center;
   color: #555;
   font-size: 0.8em;
} 
  
.center{
    text-align:center;
}  
h1{
    margin:0px;
    padding:0px;
}
.border{
    border:
}
</style>
</head>
<body>
    <header>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:0px;">
 <tr>
    <td align="left" width="30%" style="border:0px;"><img src="assets/images/logo.jpg"     qstyle="border:0px;">
</td>
    <td align="right" style="border:0px;"><h3>Ficha de Levantamiento de Información </h3></td>
 </tr> 
</table>    
    </header>
        <?php $ii=0; ?>        
        <table  border="0" width="100%"  style="border:0px;">
        <tbody>
        <tr>
            <td height="25" width="40"></td>
            <td height="25" align="right"> Fecha {{$fecha}}</td>
        </tr> 
        <tr>
            <td height="25" width="40">Dependencia</td>
            <td height="25">Dirección general de la tecnologia de la información</td>
        </tr> 
        <tr>   
            <td height="25">Responsable</td>
            <td height="25">RICARDO JORGECHAGUA SAAVEDRA</td>
        </tr>   
        <tr>
            <td height="25">Ubicación</td>
            <td height="25">Jr. Arequipa 152 - Ayacucho</td>
        </tr>    
        <tr>
            <td heighlat="25">MODALIDAD</td>
            <td height="25">FUNCIONARIO</td>
        </tr>    
     
        </tbody>

</table>
<table class="table table-bordered table-striped table-sm" width="100%">
    <thead>
        <tr>
            <th>N°</th>
            <th class="center">Codigo Patrimonio</th>
            <th>Codigo Iterno</th>
            <th  class="center">Descripcion</th>
            <th class="center">Marca</th>
            <th  class="center">Modelo</th>
            <th class="center">Tipo</th>
            <th class="center">Color</th>
            <th class="center">Serie Dimensiones</th>
            <th class="center">Estado</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($registers as $bienes)
        <?php $ii++ ?>    
            <tr>
                <td class="center border-bottom">{{ $loop->iteration }}</td>
                <td class="center border-bottom">{{ $bienes->heritage_code }}</td>
                <td class="center border-bottom">{{ $bienes->internal_code}}</td>
                <td class="center border-bottom">{{ $bienes->description }}</td>
                <td class="center border-bottom">{{ $bienes->brand }}</td>                
                <td class="center border-bottom">{{ $bienes->model}}</td>                                
                <td valign="middle" class="center border-bottom">{{ $bienes->kind }}</td>
                <td valign="middle" class="center border-bottom">{{ $bienes->color }}</td>
                <td valign="middle" class="center border-bottom">{{ $bienes->serie }}</td>
                <td valign="middle" class="center border-bottom">{{ $bienes->state }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
<div style="margin-top:20px;border-top:1px solid #ddd;padding-top:10px;font-size:12px;">
<b>Nota:</b>El Usuario declara haber mostrado todos los bienes que se encuentra bajo su responsabilidad y no contar con mas bienes materia de inventario
     El usuario es responsable de la permanencia y conservación  de cada uno de los bienes descritos recomendandosele tomar las providencias del caso para evitar perdidas, sustracciones, deterioros,etc. 
     Cualquier traslado del bien dentro o Fuera del local de la entidad, debe ser comunicado oportunamente al encargado de la UCP bajo responsabilidad.
</div>
<footer>
    <table width="100%">
      <tr>
        <td class="izquierda" width="90%">DIRECCION:<br/>Telefonos: </td>  
        <td width="10%" align="center">N° PAGINA  <span class="page-number"></span></td>  
      </tr>        
    </table>
</footer>
</body>
</html>

