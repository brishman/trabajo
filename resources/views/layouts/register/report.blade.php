<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">	
	<title>Reporte de Recepcion y Entrega de Cargo </title>
<style type="text/css">
  @page {
    margin: 80px 20px 55px 20px;
}  

 body {
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
            font-size: 0.875rem;
            font-weight: normal;
            color: #151b1e;           
        }
            header {
                position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                text-align: center;

            }
 header,footer {
                position: fixed; 
                bottom: 0px; 
                left: 0px; 
                right: 0px;
                color:#000;
                text-align: center;
                font-size:12px;
                font-family:verdana; 
}   
table {
   width: 100%;
   text-align: left;
   border-collapse: collapse;
   caption-side: top;
}
caption, td,  {
   padding: 0.3em;
}
tbody {
   border-top: 1px solid #ddd;
   border-bottom: 1px solid #ddd;
}
tbody th, tfoot th {
   border: 0;
}
th{
    font-size:12px;
    padding:5px;
    background:#ddd;
    border-bottom: 1px solid #ddd;
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;  
}
th.name {
   width: 25%;
}
th.location {
   width: 20%;
}
th.lasteruption {
   width: 30%;
}
th.eruptiontype {
   width: 25%;
}
tfoot {
   text-align: center;
   color: #555;
   font-size: 0.8em;
} 
  
.center{
    text-align:center;
}  
h1{
    margin:0px;
    padding:0px;
}
.border{
    border:
}
</style>
		<link rel="stylesheet" href="/www/public/assets/vendor/bootstrap/css/bootstrap.css" />
  
</head>
<body>
    <header>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:0px;">
 <tr>
    <td align="left" width="30%" style="border:0px;"><img src="assets/images/logo.jpg" width="50" style="border:0px;">
</td>
    <td align="right" style="border:0px;"><h3>Reporte de Recepcion y Entrega de Cargo </h3></td>
 </tr> 
</table>    
    </header>
        <?php $ii=0; ?>        
        <table  border="0" width="100%"  style="border:0px;">
        <tbody>
        <tr>
            <td height="25" width="40">Dependencia</td>
            <td height="25">Dirección general de la tecnologia de la información</td>
        </tr> 
        <tr>   
            <td height="25">Responsable</td>
            <td height="25">RICARDO JORGECHAGUA SAAVEDRA</td>
        </tr>   
        <tr>
            <td height="25">Ubicación</td>
            <td height="25">Jr. Arequipa 152 - Ayacucho</td>
        </tr>     
        <tr>
            <td heighlat="25">MODALIDAD</td>
            <td height="25">FUNCIONARIO</td>
        </tr>    
     
        </tbody>

</table>
<table class="table table-bordered table-striped table-sm" width="100%">
    <thead>
        <tr>
            <th>N°</th>
            <th>Codigo</th>
            <th class="center">Codigo Patrimonio</th>
            <th  class="center">Descripcion</th>
            <th class="center">Pecosa</th>
            <th  class="center">Estado</th>
            <th class="center">Und</th>
            <th class="center">Cantidad</th>
            
        </tr>
    </thead>
    <tbody>

        @foreach ($registers as $bienes)
        <?php $ii++ ?>    
            <tr>
                <td class="center">{{ $loop->iteration }}</td>
               <td class="center">{{ $bienes->internal_code}}</td>
                <td class="center">{{ $bienes->heritage_code }}</td>
                <td class="center">{{ $bienes->description }}</td>
                <td class="center">{{ $bienes->freckled }}</td>                
                <td class="center">{{ $bienes->state}}</td>                                
                <td valign="middle" class="center">{{ $bienes->unity }}</td>
                <td valign="middle" class="center">{{ $bienes->quantity }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
<footer>
    <table width="100%">
      <tr>
        <td class="izquierda" width="90%">DIRECCION:<br/>Telefonos: </td>  
        <td width="10%" align="center">N° PAGINA  <span class="page-number"></span></td>  
      </tr>        
    </table>
</footer>
</body>
</html>

