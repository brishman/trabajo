export const deletable = {
    methods: {
        destroy(url,mensaje,form) {
            return new Promise((resolve) => {
                this.$confirm(mensaje, 'Aviso', {
                    confirmButtonText: 'aceptar',
                    cancelButtonText: 'Cancelar',
                    type: 'warning'
                }).then(() => {
                    this.$http.post(url,form)
                        .then(res => {
                            if(res.data.success) {
                                this.$message.success(res.data.msg)
                                resolve()
                            }
                        })
                        .catch(error => {
                            if (error.response.status === 500) {
                                this.$message.error('Error al intentar dar de baja');
                            } else {
                                console.log(error.response.data.message)
                            }
                        })
                }).catch(error => {
                    console.log(error)
                });
            })
        },
    }
}