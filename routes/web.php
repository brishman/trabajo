<?php
Route::group(['middleware'=>['guest']], function () {
    Route::get('/', 'Auth\LoginController@showLoginForm');
    Route::post('/login','Auth\LoginController@login');
	Route::get('/login','Auth\LoginController@showLoginForm')->name('login');
});
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware'=>['auth']], function () {
//Route::get('/', 'Auth\LoginController@showLoginForm');
//Route::get('/login', 'Auth\LoginController@login');
Route::get('/registers/tables', 'RegisterController@tables');
Route::get('/registers/reports/{tipo}/{fecha1}/{fecha2}', 'RegisterController@report');
Route::get('/registers', 'RegisterController@index');
Route::post('/registers', 'RegisterController@save');
Route::get('/registers/columns', 'RegisterController@columns');
Route::get('/registers/records', 'RegisterController@records');
Route::get('/registers/record/{item}', 'RegisterController@record');
Route::post('/registers/{accion}/{item}', 'RegisterController@baja');

Route::post('/personal', 'PersonalController@store');
Route::get('/personal', 'PersonalController@index');
Route::get('/personal/columns', 'PersonalController@columns');
Route::get('/personal/records', 'PersonalController@records');
Route::get('/personal/record/{item}', 'PersonalController@record');
Route::post('/personal/baja', 'PersonalController@baja');
});
//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
