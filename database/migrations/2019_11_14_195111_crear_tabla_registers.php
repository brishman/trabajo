<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaRegisters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->string('dependence',250);
            $table->string('username',250);
            $table->string('location',250);
            $table->string('modality',250);
            $table->string('internal_code',250);
            $table->string('heritage_code',250);
            $table->string('description',250);
            $table->string('freckled',250);
            $table->string('unity',250);
            $table->string('quantity');
            $table->string('state',250);   
            $table->string('brand',250);
            $table->string('model');
            $table->string('kind',250);
            $table->string('color',250);
            $table->string('serie',250);
            $table->decimal('price',12,4);
            $table->decimal('total',12,4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registers');
    }
}
