<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Admininstrador',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123'),
            'type'=>'1'
        ]);
    }
}
